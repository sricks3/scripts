#!/bin/bash

# Takes in an audio file and creates new files from the first 1, 2, 4, 8, 12,
# and 16 seconds.
#
# This script assumes that the input file is an audio file

if [ $# -eq 0 ] # Check for at least 1 input file
then
  echo "No input file given."
  exit 1
elif ! [ -r $1 ] # Check that file exists and is readable
then
  echo "Cannot find or read file."
  exit 2
else # Get file and extension
  INFILE=$1
  FILENAME="${INFILE##*/}"
  EXT="${FILENAME##*.}"
fi

# Array of clip lengths
CLIPS=( 1 2 4 8 12 16 )

# Create clips of lengths
for i in "${CLIPS[@]}"
do
  TIME=$((i+1))
  NEWFILE="$i.$EXT"
  ffmpeg -t $TIME -i $INFILE -acodec copy $NEWFILE
done
