#!/bin/bash
# WARNING: This is a work in progress. Use it carefully.

# This script unzips every .zip file from THE CURRENT directory
# into a new directory defined by $TARGETDIR.

# Where should the unzipped files go?
TARGETDIR=batchunzip

# Does $TARGETDIR exist? If not, create it.
if [ -d $TARGETDIR ]; then
	echo "The directory $TARGETDIR already exists here. I'll just place everything there."
else
	mkdir $TARGETDIR
fi

# Unzip all .zip files into $TARGETDIR.
for i in *.zip; do
	cd $TARGETDIR
	unzip "../$i"
	cd ..
done
