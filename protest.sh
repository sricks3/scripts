#!/usr/bin/env bash

####################################################################
##
## !!!ALERT!!!
##
## REMOVE THIS COMMENT BLOCK WHEN COMPLETE!
##
## This is an experiment to test out and learn some of the
## techniques laid out in the article at
## https://sharats.me/posts/shell-script-best-practices/
##
####################################################################

set -o errexit
set -o nounset
set -o pipefail
if [[ "${TRACE-0}" == "1" ]]; then
    set -o xtrace
fi

if [[ "${1-}" =~ ^-*h(elp)?$ ]]; then
    echo 'Usage: ./protest.sh

This is an experiment to test out and learn from of the
techniques laid out in the article at
https://sharats.me/posts/shell-script-best-practices/

'
    exit
fi

cd "$(dirname "$0")"

main() {
    echo 'You have successfully executed protest.sh.'
}

main "$@"
